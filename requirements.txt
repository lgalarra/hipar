pandas>=1.1.5
numpy>=1.19
scipy>=1.5.4
scikit-learn>=0.23
cython
mdlp-discretization @ git+https://github.com/hlin117/mdlp-discretization
ortools>=8.1
sortedcontainers>=2.3.0
