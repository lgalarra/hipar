from hipar import data
from hipar.hipar import HIPAR
from hipar.output import format_for_output

if __name__ == '__main__':
    X, y = data.get_cpu()
    print(X.head())
    hipar = HIPAR(n_jobs=1, min_support=0.05)
    hipar.fit(X, y)
    print(format_for_output(hipar.all_rules))