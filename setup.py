#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import find_packages, setup
import hipar

DISTNAME = 'hipar'
DESCRIPTION = 'Hierarchical Pattern-aided Regression'
MAINTAINER = 'Luis Galárraga'
MAINTAINER_EMAIL = 'luis.galarraga@inria.fr'
URL = 'https://gitlab.inria.fr/lgalarra/hipar'
LICENSE = 'BSD 3-Clause'
DOWNLOAD_URL = 'https://gitlab.inria.fr/lgalarra/hipar'
VERSION = hipar.__version__
CLASSIFIERS = ['Development Status :: 3 - Alpha',
               'Intended Audience :: Science/Research',
               'Intended Audience :: Developers',
               'License :: OSI Approved',
               'Programming Language :: Python',
               'Topic :: Software Development',
               'Topic :: Scientific/Engineering',
               'Operating System :: Microsoft :: Windows',
               'Operating System :: POSIX',
               'Operating System :: Unix',
               'Operating System :: MacOS',
               'Programming Language :: Python :: 3.8',
               'Programming Language :: Python :: 3 :: Only']

KEYWORDS='regression, pattern mining'
PYTHON_REQUIRES='>=3.8, <4'
EXTRAS_REQUIRE = {
    'tests': ['pytest'],
    'docs': [
        'sphinx',
        'sphinx-gallery',
        'sphinx_rtd_theme',
        'numpydoc',
        'matplotlib'
    ]
}


# try replacing with `codecs.open('README.md', encoding='utf-8-sig') as f:` if error
with open('README.md') as readme_file:
    LONG_DESCRIPTION = readme_file.read()

with open('requirements.txt') as req_fd:
    INSTALL_REQUIRES = req_fd.read().splitlines()


setup(name=DISTNAME,
      maintainer=MAINTAINER,
      maintainer_email=MAINTAINER_EMAIL,
      description=DESCRIPTION,
      license=LICENSE,
      url=URL,
      version=VERSION,
      download_url=DOWNLOAD_URL,
      long_description=LONG_DESCRIPTION,
      long_description_content_type='text/markdown',
      keywords=KEYWORDS,
      zip_safe=False,  # the package can run out of an .egg file
      classifiers=CLASSIFIERS,
      packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
      install_requires=INSTALL_REQUIRES,
      extras_require=EXTRAS_REQUIRE,
      package_dir={'hipar' : 'hipar'},
      package_data={'hipar': ['resources/datasets/cpu.data', 'resources/datasets/winequality']},
      include_package_data=True,
      python_requires=PYTHON_REQUIRES# Optional
)