"""
HiPaR
Hierarchical Pattern-aided Regression
"""

__author__ = """Luis Galárraga"""
__email__ = """luis.galarraga@inria.fr"""
__version__ = "0.2.0"  # pylint: disable= undefined-variable, pointless-statement
__credits__ = "Centre Inria Rennes - Bretagne Atlantique"