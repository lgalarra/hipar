import unittest
import pandas as pd
import time
import numpy as np

from sklearn.linear_model import LassoCV
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.datasets import make_regression

from hipar.discretize import DefaultHIPARDiscretizer
from ..data import get_simple_housing, get_cpu, get_wine_quality, get_ifv_vine_diseases
from ..hipar import HIPAR
from ..rule_selection import TrivialHiPaRRuleSelector, DefaultHiPaRRuleSelector

import warnings

class HiPaRTestCases(unittest.TestCase):

    def test_closed_patterns(self) :
        print('test_closed_patterns')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        hipar = HIPAR(rule_selector=None, min_support=2, enable_pruning_interclass_variance=False, debug=True)
        X, y = get_simple_housing()
        print(X)
        hipar.fit(X, y, excluded_attributes_in_conditions=['rooms', 'surface'])
        rules = hipar.all_rules
        pd.set_option('display.max_columns', None)
        print(rules)
        self.assertTrue(rules['condition'].isin([(('property-type', 'cottage'), )]).any())
        self.assertTrue(rules['condition'].isin([(('property-type', 'appartment'), )]).any())

    def test_depth_limit(self) :
        print('test_depth_limit')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        hipar = HIPAR(rule_selector=None, min_support=2, enable_pruning_interclass_variance=False, debug=True, max_depth=1)
        X, y = get_simple_housing()
        print(X)
        hipar.fit(X, y, excluded_attributes_in_conditions=['rooms', 'surface'])
        rules = hipar.all_rules
        pd.set_option('display.max_columns', None)
        print(rules)
        for idx, row in rules.iterrows():
            assert(len(row['condition']) < 2)

    def test_selection(self) :
        print('test_selection')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        hipar = HIPAR(min_support=2)
        X, y = get_simple_housing()
        hipar.fit(X, y)
        pd.set_option('display.max_columns', None)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)


    def test_prediction(self) :
        print('test_prediction')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        hipar = HIPAR(min_support=2)
        X, y = get_simple_housing()
        hipar.fit(X, y)
        print(hipar.get_selected_rules())
        Xt = pd.DataFrame({'property-type' : ['cottage', 'appartment', 'cottage'],
        'state': ['v. good', 'good', 'excellent'],
        'rooms' : [5, 2, 1],
        'surface' : [115, 57, 45]})
        print(hipar.predict(Xt))

    def test_prediction_with_weights(self) :
        print('test_prediction_with_weights')
        hipar = HIPAR(min_support=2)
        X, y, weights = get_simple_housing(with_weights=True)
        hipar.fit(X, y, sample_weight=weights)
        print(hipar.get_selected_rules())
        Xt = pd.DataFrame({'property-type' : ['cottage', 'appartment', 'cottage'],
        'state': ['v. good', 'good', 'excellent'],
        'rooms' : [5, 2, 1],
        'surface' : [115, 57, 45]})
        print(hipar.predict(Xt))

    def test_symbolic_arguments_in_default_model(self):
        print('test_symbolic_arguments_in_default_model')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        hipar = HIPAR(min_support=2, symbolic_attributes_in_regression_models=True)
        X, y = get_simple_housing()
        hipar.fit(X, y)
        print(hipar.get_selected_rules())
        Xt = pd.DataFrame({'property-type': ['cottage', 'appartment', 'cottage'],
                           'state': ['v. good', 'good', 'excellent'],
                           'rooms': [5, 2, 1],
                           'surface': [115, 57, 45]})
        print(hipar.predict(Xt))

    def test_on_cpu_all_rules(self) :
        print('test_on_cpu_all_rules')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar_all_rules = HIPAR(rule_selector=TrivialHiPaRRuleSelector(),
                                min_support=0.02, debug=True)
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        hipar_all_rules.fit(X_train, y_train)
        selected_rules = hipar_all_rules.get_selected_rules()
        print('All rules')
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar_all_rules.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar (All rules) MSE=', mse, 'R2 score=', r2s)

        y_predicted_default = hipar_all_rules.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default)
        print('Default model MSE=', mse, 'R2 score=', r2s)

        hipar = HIPAR(min_support=0.02, debug=True)
        hipar.fit(X_train, y_train)
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)

    def test_depth_limit_on_cpu(self):
        print('test_depth_limit_on_cpu')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar_limit = HIPAR(min_support=0.02, max_depth=2)
        hipar_classic = HIPAR(min_support=0.02)
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        hipar_limit.fit(X_train, y_train)
        hipar_classic.fit(X_train, y_train)
        y_predicted = hipar_limit.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar max depth of 2, MSE=', mse, 'R2 score=', r2s)

        y_predicted_default = hipar_limit.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default)
        print('Default model MSE=', mse, 'R2 score=', r2s)

        y_predicted = hipar_classic.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)



    def test_on_cpu_all_rules_ommit_attributes_in_conditions(self) :
        print('test_on_cpu_all_rules_ommit_attributes_in_conditions')
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        pd.set_option('display.max_columns', None)
        hipar = HIPAR(min_support=0.1, debug=True, rule_selector=TrivialHiPaRRuleSelector())
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        omitted_attributes = ['CACH', 'CHMIN', 'Model_Name']
        hipar.fit(X_train, y_train, excluded_attributes_in_conditions=omitted_attributes)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        for idx, rule_row in selected_rules.iterrows() :
            condition = rule_row['condition']
            for attr in omitted_attributes :
                self.assertTrue(attr not in [c[0] for c in condition])
                for c in condition :
                    self.assertTrue(attr not in c[0])

        y_predicted = hipar.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted_default = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default)
        print('Default model MSE=', mse, 'R2 score=', r2s)


    def test_on_cpu(self) :
        print('test_on_cpu')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        hipar = HIPAR(min_support=0.1, debug=False, regression_class=LassoCV)
        hipar.fit(X_train, y_train)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar.predict(X_test, debug=False)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted_default = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default)
        print('Default model MSE=', mse, 'R2 score=', r2s)

        hipar2 = HIPAR(min_support=0.1, debug=False, discretize_target='abs_error')
        hipar2.fit(X_train, y_train)
        selected_rules = hipar2.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar2.predict(X_test, debug=False)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)

        hipar3 = HIPAR(min_support=0.1, debug=False, discretize_target='error')
        hipar3.fit(X_train, y_train)
        selected_rules = hipar3.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar3.predict(X_test, debug=False)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)


    def test_on_cpu_with_modified_discretization(self) :
        print('test_on_cpu_with_modified_discretization')
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(min_support=0.1, debug=True, discretizer_class=DefaultHIPARDiscretizer,
                      discretizer_args={'force_discretization': True, 'percentile_cut': 50, 'min_support': 15})
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        hipar.fit(X_train, y_train)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted_default = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_cpu_with_weights(self) :
        print('test_on_cpu_with_weights')
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(min_support=0.1, debug=True)
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        weights_train = pd.Series(np.ones(len(y_train)), index=y_train.index)
        weights_test = pd.Series(np.ones(len(y_test)), index=y_test.index)
        weights_train.iloc[0] = 2
        weights_train.iloc[1] = 2
        weights_train.iloc[2] = 2
        hipar.fit(X_train, y_train, sample_weight=weights_train)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted, sample_weight=weights_test)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted_default = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default, sample_weight=weights_test)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_cpu_no_filtering_iv(self) :
        print('test_on_cpu_no_filtering_iv')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        warnings.filterwarnings("ignore", category=FutureWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(min_support=0.3, debug=True, interclass_variance_percentile_threshold=0, simplified_interclass_variance=False)
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        hipar.fit(X_train, y_train)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted_default = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_cpu_no_filtering_iv_2(self) :
        print('test_on_cpu_no_filtering_iv_2')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(min_support=0.3, debug=True, n_jobs=4, enable_pruning_interclass_variance=False)
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        hipar.fit(X_train, y_train)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted_default = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_test, y_predicted_default)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_wine_quality_symbolic_attributes_in_default_model(self) :
        print('test_on_wine_quality_symbolic_attributes_in_default_model')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)

        X, y = get_wine_quality()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        hipar = HIPAR(min_support=0.02, debug=True)
        hipar_symbolic_arguments_in_default_model = HIPAR(min_support=0.02, symbolic_attributes_in_regression_models=True,
                                                          debug=True)
        hipar.fit(X_train, y_train)
        hipar_symbolic_arguments_in_default_model.fit(X_train, y_train)
        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

        y_predicted = hipar_symbolic_arguments_in_default_model.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar (symbolic attributes in DM) MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar_symbolic_arguments_in_default_model.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model (symbolic attributes in DM) MSE=', mse, 'R2 score=', r2s)

    def test_on_wine_quality_error_discr(self):
        print('test_on_wine_quality_error_discr')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)

        X, y = get_wine_quality()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        hipar = HIPAR(min_support=0.05, debug=True, n_jobs=8)
        hipar2 = HIPAR(min_support=0.05, debug=True, discretize_target='error', n_jobs=8)
        hipar3 = HIPAR(min_support=0.05, debug=True, discretize_target='abs_error', n_jobs=8)

        hipar.fit(X_train, y_train)
        hipar2.fit(X_train, y_train)
        hipar3.fit(X_train, y_train)

        selected_rules = hipar.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

        y_predicted = hipar2.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar (error discr) MSE=', mse, 'R2 score=', r2s)

        y_predicted = hipar3.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar (abs error discr) MSE=', mse, 'R2 score=', r2s)

    def test_on_wine_quality_simplified_iv(self) :
        print('test_on_wine_quality_simplified_iv')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)

        X, y = get_wine_quality()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        hipar_simplified_iv = HIPAR(min_support=0.02, debug=True)
        hipar_full_iv = HIPAR(min_support=0.02, debug=True, simplified_interclass_variance=False)
        hipar_simplified_iv.fit(X_train, y_train)
        hipar_full_iv.fit(X_train, y_train)

        selected_rules = hipar_full_iv.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        y_predicted = hipar_full_iv.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar full IV MSE=', mse, 'R2 score=', r2s)

        y_predicted = hipar_simplified_iv.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar simplified IV MSE=', mse, 'R2 score=', r2s)

    def test_on_ifv_vine_diseases(self):
        print('test_on_ifv_vine_diseases')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(n_jobs=4, debug=True)
        X, y = get_ifv_vine_diseases(simplified=True, sample_size=2000)
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        startT = time.time()
        hipar.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())
        y_predicted = hipar.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

        hipar2 = HIPAR(n_jobs=4, debug=True, discretize_target='abs_error')
        startT = time.time()
        hipar2.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar2.get_selected_rules())
        y_predicted = hipar2.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar2.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

        hipar3 = HIPAR(n_jobs=4, debug=True, discretize_target='error')
        startT = time.time()
        hipar3.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar3.get_selected_rules())
        y_predicted = hipar3.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar3.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_ifv_vine_diseases_max_support(self):
        print('test_on_ifv_vine_diseases')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(n_jobs=4, debug=True, max_support=0.6)
        X, y = get_ifv_vine_diseases(simplified=True, sample_size=2000)
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        startT = time.time()
        hipar.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())
        y_predicted = hipar.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_ifv_vine_diseases_ommit_attrs(self):
        print('test_on_ifv_vine_diseases_ommit_attrs')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(n_jobs=4, debug=True)
        X, y = get_ifv_vine_diseases(simplified=True, sample_size=1000)
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        startT = time.time()
        ommited_attrs = ['mfi_avg_4w']
        hipar.fit(X_train, y_train, excluded_attributes_in_conditions=ommited_attrs)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())

    def test_on_ifv_vine_diseases_all_rules(self):
        print('test_on_ifv_vine_diseases_all_rules')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(n_jobs=2, debug=True, rule_selector=TrivialHiPaRRuleSelector())
        X, y = get_ifv_vine_diseases(simplified=True, remove_zeros=False)
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        print('Training on', len(X_train), 'instances')
        startT = time.time()
        hipar.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_ifv_vine_diseases_all_rules_parallel_closure(self):
        print('test_on_ifv_vine_diseases_all_rules_parallel_closure')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(debug=True, rule_selector=DefaultHiPaRRuleSelector(), parallel_closure_exec=True)
        X, y = get_ifv_vine_diseases(simplified=True)
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.5)
        startT = time.time()
        hipar.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)


    def test_on_ifv_vine_diseases_all_rules_2_jobs(self):
        print('test_on_ifv_vine_diseases_all_rules_2_jobs')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(debug=True, rule_selector=TrivialHiPaRRuleSelector(), n_jobs=2)
        X, y = get_ifv_vine_diseases(simplified=True)
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        startT = time.time()
        hipar.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())


    def test_on_ifv_vine_diseases_symbolic_attributes_in_default_model(self):
        print('test_on_ifv_vine_diseases_symbolic_attributes_in_regression_models')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(n_jobs=4, debug=True, symbolic_attributes_in_regression_models=True)
        X, y = get_ifv_vine_diseases(simplified=True, sample_size=1000)
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        startT = time.time()
        hipar.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())
        y_predicted = hipar.predict(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model (symbolic attributes in DM) MSE=', mse, 'R2 score=', r2s)

    def test_on_ifv_vine_diseases_lasso_cv(self):
        print('test_on_ifv_vine_diseases')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        hipar = HIPAR(n_jobs=4, debug=True, regression_class=LassoCV)
        X, y = get_ifv_vine_diseases(simplified=True, sample_size=1000)
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        startT = time.time()
        hipar.fit(X_train, y_train)
        print('Training took (time)', (time.time() - startT), 'seconds')
        print(hipar.get_selected_rules())
        y_predicted = hipar.predict(X_test, debug=True)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Hipar MSE=', mse, 'R2 score=', r2s)
        y_predicted = hipar.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_on_cpu_with_a_posteriori_selection(self):
        print('test_on_cpu_with_a_posteriori_selection')
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        pd.set_option('display.max_columns', None)
        ## Use the trivial rule selector that does nothing
        hipar_all_rules = HIPAR(rule_selector=TrivialHiPaRRuleSelector(),
                                min_support=0.1, debug=True)
        X, y = get_cpu()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        hipar_all_rules.fit(X_train, y_train)
        selected_rules = hipar_all_rules.get_selected_rules()
        print(selected_rules)
        self.assertTrue(len(selected_rules) > 0)
        ## Use a customized rule selector
        y_predicted_default = hipar_all_rules.predict(X_test,
                                                      rule_selector=DefaultHiPaRRuleSelector(),
                                                      debug=True)
        mse = mean_squared_error(y_test, y_predicted_default)
        r2s = r2_score(y_predicted_default, y_test)
        print('Hipar (rules with sigma=1.0, omega=1.0)', mse, 'R2 score=', r2s)

        y_predicted = hipar_all_rules.predict_with_default_model(X_test)
        mse = mean_squared_error(y_test, y_predicted)
        r2s = r2_score(y_test, y_predicted)
        print('Default model MSE=', mse, 'R2 score=', r2s)

    def test_exhaustivity(self) :
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        X, y = make_regression(n_features=1, noise=0, n_samples=1000)
        y = pd.Series(y)
        X = pd.DataFrame(X, columns=["a"])

        X2, y2 = make_regression(n_features=1, noise=0, n_samples=1000)
        y2 = pd.Series(y2)
        X2 = pd.DataFrame(X2, columns=["a"])

        X3 = X.append(X2)
        y3 = y.append(y2)
        X3 = X3.reset_index(drop=True)
        y3 = y3.reset_index(drop=True)

        one = np.full(1000, '0')

        zero = np.full(1000, '1')

        classe = np.concatenate((one, zero))

        X3['f'] = classe
        print(X3.head())
        print(X3.tail())

        for i in range(0, 10):
            X_train, X_test, y_train, y_test = train_test_split(X3, y3, train_size=0.7)

            hipar = HIPAR(rule_selector=TrivialHiPaRRuleSelector(), min_support=0.02, debug=True,
                          enable_pruning_interclass_variance=False)

            hipar.fit(X_train, y_train)
            selected_rules = hipar.get_selected_rules()
            print('All rules')
            print(selected_rules)
            y_pred = hipar.predict(X_test)
            mse = mean_squared_error(y_test, y_pred, squared=False)
            print('Full model MSE ext =', mse)
            y_pred = hipar.predict_with_default_model(X_test)
            mse = mean_squared_error(y_test, y_pred, squared=False)
            print('Default model MSE ext =', mse)
            #self.assertTrue(selected_rules['condition'].isin([(('f', '1'), ), (('f', '0'), )]).any())




if __name__ == '__main__':
    unittest.main()
