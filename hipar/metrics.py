#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 4 17:54:00 2021

@author: lgalarra
"""
import numpy as np

from sklearn.metrics import mean_squared_error, median_absolute_error, r2_score

smaller_the_better = lambda x, y: x < y
larger_the_better = lambda x, y: x > y

metric_comparators_dict = {mean_squared_error: smaller_the_better,
                           median_absolute_error: smaller_the_better,
                           r2_score: larger_the_better}


def better_performance(metric, v1, v2):
    return metric_comparators_dict[metric](v1, v2)


def interclass_variance(tids, full_index_y_set, y, global_mean, simplified=True):
    pattern_mean = np.mean(y.loc[tids])
    if simplified:
        return len(tids) * pow(pattern_mean - global_mean, 2)
    else:
        tids_c = list(full_index_y_set.difference(set(tids)))
        pattern_complement_mean = np.mean(y.loc[tids_c])
        return len(tids) * pow(pattern_mean - global_mean, 2) + \
               len(tids_c) * pow(pattern_complement_mean - global_mean, 2)

def group_interclass_variance(items, y, global_mean, simplified=True):
    ivs = []
    full_index_y_set = set(y.index)
    if simplified:
        for item in items:
            tids = item[1]
            pattern_mean = np.mean(y.loc[tids])
            ivs.append(len(tids) * pow(pattern_mean - global_mean, 2))
    else:
        for item in items:
            tids = item[1]
            pattern_mean = np.mean(y.loc[tids])
            tids_c = list(full_index_y_set.difference(set(tids)))
            pattern_complement_mean = np.mean(y.loc[tids_c])
            ivs.append(
                len(tids) * pow(pattern_mean - global_mean, 2) +
                len(tids_c) * pow(pattern_complement_mean - global_mean, 2)
            )

    return ivs
